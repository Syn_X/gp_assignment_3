#include "stdafx.h"
#include "textures_demo_app.h"
#include <service/locator.h>
#include <file/file_utils.h>
#include <math/math_utils.h>

using namespace DirectX;
using namespace xtest;

using xtest::demo::TexturesDemoApp;
using Microsoft::WRL::ComPtr;

TexturesDemoApp::TexturesDemoApp(HINSTANCE instance,
	const application::WindowSettings& windowSettings,
	const application::DirectxSettings& directxSettings,
	uint32 fps /*=60*/)
	: application::DirectxApp(instance, windowSettings, directxSettings, fps)
	, m_viewMatrix()
	, m_projectionMatrix()
	, m_camera(math::ToRadians(68.f), math::ToRadians(135.f), 7.f, { 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f }, { math::ToRadians(4.f), math::ToRadians(175.f) }, { 3.f, 25.f })
	, m_dirLight()
	, m_spotLight()
	, m_pointLights()
	, m_sphere()
	, m_plane()
	, m_torus()
	, m_lightsControl()
	, m_isLightControlDirty(true)
	, m_stopLights(false)
	, m_d3dPerFrameCB(nullptr)
	, m_d3dRarelyChangedCB(nullptr)
	, m_vertexShader(nullptr)
	, m_pixelShader(nullptr)
	, m_inputLayout(nullptr)
	, m_rasterizerState(nullptr)
{}


TexturesDemoApp::~TexturesDemoApp()
{}


void TexturesDemoApp::Init()
{
	application::DirectxApp::Init();
	m_d3dAnnotation->BeginEvent(L"init-demo");

	InitMatrices();
	InitShaders();
	InitRenderable();
	InitLights();
	InitRasterizerState();
	InitSamplerState();

	service::Locator::GetMouse()->AddListener(this);
	service::Locator::GetKeyboard()->AddListener(this, { input::Key::F, input::Key::F1, input::Key::F2, input::Key::F3, input::Key::space_bar, input::Key::one, input::Key::two, input::Key::three });

	m_d3dAnnotation->EndEvent();
}

void TexturesDemoApp::InitMatrices()
{
	// view matrix
	XMStoreFloat4x4(&m_viewMatrix, m_camera.GetViewMatrix());

	// projection matrix
	{
		XMMATRIX P = XMMatrixPerspectiveFovLH(math::ToRadians(45.f), AspectRatio(), 1.f, 1000.f);
		XMStoreFloat4x4(&m_projectionMatrix, P);
	}
}

void TexturesDemoApp::InitShaders()
{
	// read pre-compiled shaders' bytecode
	std::future<file::BinaryFile> psByteCodeFuture = file::ReadBinaryFile(std::wstring(GetRootDir()).append(L"\\textures_demo_PS.cso"));
	std::future<file::BinaryFile> vsByteCodeFuture = file::ReadBinaryFile(std::wstring(GetRootDir()).append(L"\\textures_demo_VS.cso"));

	// future.get() can be called only once
	file::BinaryFile vsByteCode = vsByteCodeFuture.get();
	file::BinaryFile psByteCode = psByteCodeFuture.get();
	XTEST_D3D_CHECK(m_d3dDevice->CreateVertexShader(vsByteCode.Data(), vsByteCode.ByteSize(), nullptr, &m_vertexShader));
	XTEST_D3D_CHECK(m_d3dDevice->CreatePixelShader(psByteCode.Data(), psByteCode.ByteSize(), nullptr, &m_pixelShader));


	// create the input layout, it must match the Vertex Shader HLSL input format:
	D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(mesh::MeshData::Vertex, normal), D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(mesh::MeshData::Vertex, tangentU), D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(mesh::MeshData::Vertex, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	XTEST_D3D_CHECK(m_d3dDevice->CreateInputLayout(vertexDesc, 4, vsByteCode.Data(), vsByteCode.ByteSize(), &m_inputLayout));


	// perFrameCB
	D3D11_BUFFER_DESC perFrameCBDesc;
	perFrameCBDesc.Usage = D3D11_USAGE_DYNAMIC;
	perFrameCBDesc.ByteWidth = sizeof(PerFrameCB);
	perFrameCBDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	perFrameCBDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	perFrameCBDesc.MiscFlags = 0;
	perFrameCBDesc.StructureByteStride = 0;
	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&perFrameCBDesc, nullptr, &m_d3dPerFrameCB));
}

void TexturesDemoApp::InitRenderable()
{
	std::wstring texturesFolder = GetRootDir().append(LR"(\3d-objects)");

	// plane
	{
		// geo
		m_plane.mesh = mesh::GeneratePlane(50.f, 50.f, 50, 50);


		// W
		XMStoreFloat4x4(&m_plane.W, XMMatrixIdentity());


		// material
		m_plane.material.ambient = { 0.15f, 0.15f, 0.15f, 1.f };
		m_plane.material.diffuse = { 0.52f, 0.52f, 0.52f, 1.f };
		m_plane.material.specular = { 0.8f, 0.8f, 0.8f, 1.f };

		// textures
		std::wstring textureName = LR"(\ground\ground)";
		// albedo
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_color.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_plane.d3dAlbedoMap = std::move(texture.d3dShaderView);
		}
		// normal
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_norm.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_plane.d3dNormalMap = std::move(texture.d3dShaderView);
		}
		// gloss
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_gloss.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_plane.d3dGlossMap = std::move(texture.d3dShaderView);
		}

		textureName = LR"(\plastic-cover\plastic_cover)";
		// skin
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_color.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_plane.d3dSkinMap = std::move(texture.d3dShaderView);
		}
		m_plane.hasSkin = false;

		// perObjectCB
		D3D11_BUFFER_DESC perObjectCBDesc;
		perObjectCBDesc.Usage = D3D11_USAGE_DYNAMIC;
		perObjectCBDesc.ByteWidth = sizeof(PerObjectCB);
		perObjectCBDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		perObjectCBDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		perObjectCBDesc.MiscFlags = 0;
		perObjectCBDesc.StructureByteStride = 0;
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&perObjectCBDesc, nullptr, &m_plane.d3dPerObjectCB));


		// vertex buffer
		D3D11_BUFFER_DESC vertexBufferDesc;
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.ByteWidth = UINT(sizeof(mesh::MeshData::Vertex) * m_plane.mesh.vertices.size());
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertexBufferDesc.CPUAccessFlags = 0;
		vertexBufferDesc.MiscFlags = 0;
		vertexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA vertexInitData;
		vertexInitData.pSysMem = &m_plane.mesh.vertices[0];
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, &m_plane.d3dVertexBuffer));


		// index buffer
		D3D11_BUFFER_DESC indexBufferDesc;
		indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		indexBufferDesc.ByteWidth = UINT(sizeof(uint32) * m_plane.mesh.indices.size());
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA indexInitdata;
		indexInitdata.pSysMem = &m_plane.mesh.indices[0];
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&indexBufferDesc, &indexInitdata, &m_plane.d3dIndexBuffer));

		// Transform matrix
		XMStoreFloat4x4(&m_plane.transformMatrix, XMMatrixIdentity());

	}

	// sphere
	{

		//geo
		m_sphere.mesh = mesh::GenerateSphere(1.f, 40, 40);


		// W
		XMStoreFloat4x4(&m_sphere.W, XMMatrixTranslation(-4.f, 1.f, 0.f));

		// material
		m_sphere.material.ambient = { 0.7f, 0.7f, 0.7f, 1.0f };
		m_sphere.material.diffuse = { 0.7f, 0.7f, 0.7f, 1.0f };
		m_sphere.material.specular = { 0.7f, 0.7f, 0.7f, 40.0f };

		// textures
		std::wstring textureName = LR"(\sci-fi\sci_fi)";
		// albedo
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_color.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_sphere.d3dAlbedoMap = std::move(texture.d3dShaderView);
		}
		// normal
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_norm.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_sphere.d3dNormalMap = std::move(texture.d3dShaderView);
		}
		// gloss
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_gloss.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_sphere.d3dGlossMap = std::move(texture.d3dShaderView);
		}

		textureName = LR"(\tiles\tiles)";
		// skin
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_color.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_sphere.d3dSkinMap = std::move(texture.d3dShaderView);
		}
		m_sphere.hasSkin = true;

		// perObjectCB
		D3D11_BUFFER_DESC perObjectCBDesc;
		perObjectCBDesc.Usage = D3D11_USAGE_DYNAMIC;
		perObjectCBDesc.ByteWidth = sizeof(PerObjectCB);
		perObjectCBDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		perObjectCBDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		perObjectCBDesc.MiscFlags = 0;
		perObjectCBDesc.StructureByteStride = 0;
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&perObjectCBDesc, nullptr, &m_sphere.d3dPerObjectCB));


		// vertex buffer
		D3D11_BUFFER_DESC vertexBufferDesc;
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.ByteWidth = UINT(sizeof(mesh::MeshData::Vertex) * m_sphere.mesh.vertices.size());
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertexBufferDesc.CPUAccessFlags = 0;
		vertexBufferDesc.MiscFlags = 0;
		vertexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA vertexInitData;
		vertexInitData.pSysMem = &m_sphere.mesh.vertices[0];
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, &m_sphere.d3dVertexBuffer));


		// index buffer
		D3D11_BUFFER_DESC indexBufferDesc;
		indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		indexBufferDesc.ByteWidth = UINT(sizeof(uint32) * m_sphere.mesh.indices.size());
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA indexInitdata;
		indexInitdata.pSysMem = &m_sphere.mesh.indices[0];
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&indexBufferDesc, &indexInitdata, &m_sphere.d3dIndexBuffer));

		// Transform matrix
		XMStoreFloat4x4(&m_sphere.transformMatrix, XMMatrixIdentity());
	}

	// torus
	{

		//geo
		m_torus.mesh = mesh::GenerateTorus(1.0f, 0.3f, 100);

		// W
		XMStoreFloat4x4(&m_torus.W, XMMatrixTranslation(4.f, 2.f, 0.f));

		// material
		m_torus.material.ambient = { 0.7f, 0.7f, 0.7f, 1.0f };
		m_torus.material.diffuse = { 0.7f, 0.7f, 0.7f, 1.0f };
		m_torus.material.specular = { 0.7f, 0.7f, 0.7f, 40.0f };

		// textures
		std::wstring textureName = LR"(\wood\wood)";
		// albedo
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_color.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_torus.d3dAlbedoMap = std::move(texture.d3dShaderView);
		}
		// normal
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_norm.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_torus.d3dNormalMap = std::move(texture.d3dShaderView);
		}
		// gloss
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_gloss.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_torus.d3dGlossMap = std::move(texture.d3dShaderView);
		}

		textureName = LR"(\lizard\lizard)";
		// skin
		{
			std::wstring completePath = texturesFolder + textureName + LR"(_color.png)";
			xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
			m_torus.d3dSkinMap = std::move(texture.d3dShaderView);
		}
		m_torus.hasSkin = true;

		// perObjectCB
		D3D11_BUFFER_DESC perObjectCBDesc;
		perObjectCBDesc.Usage = D3D11_USAGE_DYNAMIC;
		perObjectCBDesc.ByteWidth = sizeof(PerObjectCB);
		perObjectCBDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		perObjectCBDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		perObjectCBDesc.MiscFlags = 0;
		perObjectCBDesc.StructureByteStride = 0;
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&perObjectCBDesc, nullptr, &m_torus.d3dPerObjectCB));


		// vertex buffer
		D3D11_BUFFER_DESC vertexBufferDesc;
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.ByteWidth = UINT(sizeof(mesh::MeshData::Vertex) * m_torus.mesh.vertices.size());
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertexBufferDesc.CPUAccessFlags = 0;
		vertexBufferDesc.MiscFlags = 0;
		vertexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA vertexInitData;
		vertexInitData.pSysMem = &m_torus.mesh.vertices[0];
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, &m_torus.d3dVertexBuffer));


		// index buffer
		D3D11_BUFFER_DESC indexBufferDesc;
		indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		indexBufferDesc.ByteWidth = UINT(sizeof(uint32) * m_torus.mesh.indices.size());
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA indexInitdata;
		indexInitdata.pSysMem = &m_torus.mesh.indices[0];
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&indexBufferDesc, &indexInitdata, &m_torus.d3dIndexBuffer));

		// Transform matrix
		XMStoreFloat4x4(&m_torus.transformMatrix, XMMatrixIdentity());
	}

}

void TexturesDemoApp::InitLights()
{
	m_dirLight.ambient = { 0.16f, 0.18f, 0.18f, 1.f };
	m_dirLight.diffuse = { 0.4f * 0.87f,0.4f * 0.90f,0.4f * 0.94f, 1.f };
	m_dirLight.specular = { 0.87f, 0.90f, 0.94f, 1.f };
	XMVECTOR dirLightDirection = XMVector3Normalize(-XMVectorSet(5.f, 3.f, 5.f, 0.f));
	XMStoreFloat3(&m_dirLight.dirW, dirLightDirection);

	m_pointLights[0].ambient = { 0.3f, 0.f, 0.f, 1.0f };
	m_pointLights[0].diffuse = { 0.3f, 0.f, 0.f, 1.0f };
	m_pointLights[0].specular = { 0.3f, 0.f, 0.f, 1.0f };
	m_pointLights[0].posW = { 5.f, 2.f, 5.f };
	m_pointLights[0].range = 15.f;
	m_pointLights[0].attenuation = { 0.0f, 0.2f, 0.f };

	m_pointLights[1].ambient = { 0.f, 0.3f, 0.f, 1.0f };
	m_pointLights[1].diffuse = { 0.f, 0.3f, 0.f, 1.0f };
	m_pointLights[1].specular = { 0.f, 0.3f, 0.f, 1.0f };
	m_pointLights[1].posW = { 5.f, 2.f, -5.f };
	m_pointLights[1].range = 15.f;
	m_pointLights[1].attenuation = { 0.0f, 0.2f, 0.f };

	m_pointLights[2].ambient = { 0.f, 0.f, 0.3f, 1.0f };
	m_pointLights[2].diffuse = { 0.f, 0.f, 0.3f, 1.0f };
	m_pointLights[2].specular = { 0.f, 0.f, 0.3f, 1.0f };
	m_pointLights[2].posW = { -5.f, 2.f, 5.f };
	m_pointLights[2].range = 15.f;
	m_pointLights[2].attenuation = { 0.0f, 0.2f, 0.f };

	m_pointLights[3].ambient = { 0.18f, 0.04f, 0.16f, 1.0f };
	m_pointLights[3].diffuse = { 0.94f, 0.23f, 0.87f, 1.0f };
	m_pointLights[3].specular = { 0.94f, 0.23f, 0.87f, 1.0f };
	m_pointLights[3].posW = { -5.f, 2.f, -5.f };
	m_pointLights[3].range = 15.f;
	m_pointLights[3].attenuation = { 0.0f, 0.2f, 0.f };

	m_pointLights[4].ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
	m_pointLights[4].diffuse = { 0.3f, 0.3f, 0.3f, 1.0f };
	m_pointLights[4].specular = { 0.3f, 0.3f, 0.3f, 1.0f };
	m_pointLights[4].posW = { 0.f, 2.f, 0.f };
	m_pointLights[4].range = 15.f;
	m_pointLights[4].attenuation = { 0.0f, 0.2f, 0.f };

	m_spotLight.ambient = { 0.018f, 0.018f, 0.18f, 1.0f };
	m_spotLight.diffuse = { 0.1f, 0.1f, 0.9f, 1.0f };
	m_spotLight.specular = { 0.1f, 0.1f, 0.9f, 1.0f };
	XMVECTOR posW = XMVectorSet(5.f, 5.f, -5.f, 1.f);
	XMStoreFloat3(&m_spotLight.posW, posW);
	m_spotLight.range = 50.f;
	XMVECTOR dirW = XMVector3Normalize(XMVectorSet(-4.f, 1.f, 0.f, 1.f) - posW);
	XMStoreFloat3(&m_spotLight.dirW, dirW);
	m_spotLight.spot = 40.f;
	m_spotLight.attenuation = { 0.0f, 0.125f, 0.f };


	m_lightsControl.useDirLight = false;
	m_lightsControl.usePointLight = true;
	m_lightsControl.useSpotLight = false;


	// RarelyChangedCB
	{
		D3D11_BUFFER_DESC rarelyChangedCB;
		rarelyChangedCB.Usage = D3D11_USAGE_DYNAMIC;
		rarelyChangedCB.ByteWidth = sizeof(RarelyChangedCB);
		rarelyChangedCB.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		rarelyChangedCB.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		rarelyChangedCB.MiscFlags = 0;
		rarelyChangedCB.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA lightControlData;
		lightControlData.pSysMem = &m_lightsControl;
		XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&rarelyChangedCB, &lightControlData, &m_d3dRarelyChangedCB));
	}
}

void TexturesDemoApp::InitRasterizerState()
{
	// rasterizer state
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.FrontCounterClockwise = false;
	rasterizerDesc.DepthClipEnable = true;

	m_d3dDevice->CreateRasterizerState(&rasterizerDesc, &m_rasterizerState);
}

void TexturesDemoApp::InitSamplerState() {
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	XTEST_D3D_CHECK(m_d3dDevice->CreateSamplerState(&samplerDesc, &m_textureSampler));
}

void TexturesDemoApp::OnResized()
{
	application::DirectxApp::OnResized();

	//update the projection matrix with the new aspect ratio
	XMMATRIX P = XMMatrixPerspectiveFovLH(math::ToRadians(45.f), AspectRatio(), 1.f, 1000.f);
	XMStoreFloat4x4(&m_projectionMatrix, P);
}

void TexturesDemoApp::OnWheelScroll(input::ScrollStatus scroll)
{
	// zoom in or out when the scroll wheel is used
	if (service::Locator::GetMouse()->IsInClientArea())
	{
		m_camera.IncreaseRadiusBy(scroll.isScrollingUp ? -0.5f : 0.5f);
	}
}

void TexturesDemoApp::OnMouseMove(const DirectX::XMINT2& movement, const DirectX::XMINT2& currentPos)
{
	XTEST_UNUSED_VAR(currentPos);

	input::Mouse* mouse = service::Locator::GetMouse();

	// rotate the camera position around the cube when the left button is pressed
	if (mouse->GetButtonStatus(input::MouseButton::left_button).isDown && mouse->IsInClientArea())
	{
		m_camera.RotateBy(math::ToRadians(movement.y * -0.25f), math::ToRadians(movement.x * 0.25f));
	}

	// pan the camera position when the right button is pressed
	if (mouse->GetButtonStatus(input::MouseButton::right_button).isDown && mouse->IsInClientArea())
	{
		XMFLOAT3 cameraX = m_camera.GetXAxis();
		XMFLOAT3 cameraY = m_camera.GetYAxis();

		// we should calculate the right amount of pan in screen space but for now this is good enough
		XMVECTOR xPanTranslation = XMVectorScale(XMLoadFloat3(&cameraX), float(-movement.x) * 0.01f);
		XMVECTOR yPanTranslation = XMVectorScale(XMLoadFloat3(&cameraY), float(movement.y) * 0.01f);

		XMFLOAT3 panTranslation;
		XMStoreFloat3(&panTranslation, XMVectorAdd(xPanTranslation, yPanTranslation));
		m_camera.TranslatePivotBy(panTranslation);
	}

}

void TexturesDemoApp::OnKeyStatusChange(input::Key key, const input::KeyStatus& status)
{

	// re-frame F key is pressed
	if (key == input::Key::F && status.isDown)
	{
		m_camera.SetPivot({ 0.f, 0.f, 0.f });
	}
	else if (key == input::Key::F1 && status.isDown)
	{
		m_lightsControl.useDirLight = !m_lightsControl.useDirLight;
		m_isLightControlDirty = true;
	}
	else if (key == input::Key::F2 && status.isDown)
	{
		m_lightsControl.usePointLight = !m_lightsControl.usePointLight;
		m_isLightControlDirty = true;
	}
	else if (key == input::Key::F3 && status.isDown)
	{
		m_lightsControl.useSpotLight = !m_lightsControl.useSpotLight;
		m_isLightControlDirty = true;
	}
	else if (key == input::Key::space_bar && status.isDown)
	{
		m_stopLights = !m_stopLights;
	}
	else if (key == input::Key::one && status.isDown)
	{
		m_plane.hasSkin = !m_plane.hasSkin;
	}
	else if (key == input::Key::two && status.isDown)
	{
		m_sphere.hasSkin = !m_sphere.hasSkin;
	}
	else if (key == input::Key::three && status.isDown)
	{
		m_torus.hasSkin = !m_torus.hasSkin;
	}
}

void TexturesDemoApp::UpdateScene(float deltaSeconds)
{
	XTEST_UNUSED_VAR(deltaSeconds);


	// create the model-view-projection matrix
	XMMATRIX V = m_camera.GetViewMatrix();
	XMStoreFloat4x4(&m_viewMatrix, V);

	// create projection matrix
	XMMATRIX P = XMLoadFloat4x4(&m_projectionMatrix);



	m_d3dAnnotation->BeginEvent(L"update-constant-buffer");


	// plane PerObjectCB
	{
		XMMATRIX W = XMLoadFloat4x4(&m_plane.W);
		XMMATRIX WVP = W * V * P;

		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		// disable gpu access
		XTEST_D3D_CHECK(m_d3dContext->Map(m_plane.d3dPerObjectCB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
		PerObjectCB* perObjectCB = static_cast<PerObjectCB*>(mappedResource.pData);

		//update the data
		XMStoreFloat4x4(&perObjectCB->W, XMMatrixTranspose(W));
		XMStoreFloat4x4(&perObjectCB->WVP, XMMatrixTranspose(WVP));
		XMStoreFloat4x4(&perObjectCB->W_inverseTraspose, XMMatrixInverse(nullptr, W));
		perObjectCB->material = m_plane.material;
		perObjectCB->hasSkin = m_plane.hasSkin;
		XMStoreFloat4x4(&perObjectCB->TexcoordMatrix, XMMatrixTranspose(XMLoadFloat4x4(&m_plane.transformMatrix)));

		// enable gpu access
		m_d3dContext->Unmap(m_plane.d3dPerObjectCB.Get(), 0);
	}

	// sphere PerObjectCB
	{
		XMMATRIX R = XMMatrixTranslation(math::ToRadians(10.f) * deltaSeconds, math::ToRadians(15.f) * deltaSeconds, 0.f);
		XMStoreFloat4x4(&m_sphere.transformMatrix, XMMatrixMultiply(XMLoadFloat4x4(&m_sphere.transformMatrix), R));


		XMMATRIX W = XMLoadFloat4x4(&m_sphere.W);
		XMMATRIX WVP = W * V * P;

		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		// disable gpu access
		XTEST_D3D_CHECK(m_d3dContext->Map(m_sphere.d3dPerObjectCB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
		PerObjectCB* perObjectCB = static_cast<PerObjectCB*>(mappedResource.pData);

		//update the data
		XMStoreFloat4x4(&perObjectCB->W, XMMatrixTranspose(W));
		XMStoreFloat4x4(&perObjectCB->WVP, XMMatrixTranspose(WVP));
		XMStoreFloat4x4(&perObjectCB->W_inverseTraspose, XMMatrixInverse(nullptr, W));
		perObjectCB->material = m_sphere.material;
		perObjectCB->hasSkin = m_sphere.hasSkin;
		XMStoreFloat4x4(&perObjectCB->TexcoordMatrix, XMMatrixTranspose(XMLoadFloat4x4(&m_sphere.transformMatrix)));

		// enable gpu access
		m_d3dContext->Unmap(m_sphere.d3dPerObjectCB.Get(), 0);
	}

	// torus PerObjectCB
	{
		XMMATRIX R = XMMatrixTranslation(math::ToRadians(5.f) * deltaSeconds, math::ToRadians(1.f) * deltaSeconds, 0.f);
		XMStoreFloat4x4(&m_torus.transformMatrix, XMMatrixMultiply(XMLoadFloat4x4(&m_torus.transformMatrix), R));

		XMMATRIX W = XMLoadFloat4x4(&m_torus.W);
		XMMATRIX WVP = W * V * P;

		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		// disable gpu access
		XTEST_D3D_CHECK(m_d3dContext->Map(m_torus.d3dPerObjectCB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
		PerObjectCB* perObjectCB = static_cast<PerObjectCB*>(mappedResource.pData);

		//update the data
		XMStoreFloat4x4(&perObjectCB->W, XMMatrixTranspose(W));
		XMStoreFloat4x4(&perObjectCB->WVP, XMMatrixTranspose(WVP));
		XMStoreFloat4x4(&perObjectCB->W_inverseTraspose, XMMatrixInverse(nullptr, W));
		perObjectCB->material = m_torus.material;
		perObjectCB->hasSkin = m_torus.hasSkin;
		XMStoreFloat4x4(&perObjectCB->TexcoordMatrix, XMMatrixTranspose(XMLoadFloat4x4(&m_torus.transformMatrix)));

		// enable gpu access
		m_d3dContext->Unmap(m_torus.d3dPerObjectCB.Get(), 0);
	}

	// PerFrameCB
	{

		if (!m_stopLights)
		{
			XMMATRIX R = XMMatrixRotationY(math::ToRadians(30.f) * deltaSeconds);
			for (int i = 0; i < m_pointLights.size(); i++) {
				XMStoreFloat3(&m_pointLights[i].posW, XMVector3Transform(XMLoadFloat3(&m_pointLights[i].posW), R));
			}

			R = XMMatrixRotationAxis(XMVectorSet(-1.f, 0.f, 1.f, 1.f), math::ToRadians(10.f) * deltaSeconds);
			XMStoreFloat3(&m_dirLight.dirW, XMVector3Transform(XMLoadFloat3(&m_dirLight.dirW), R));
		}

		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		// disable gpu access
		XTEST_D3D_CHECK(m_d3dContext->Map(m_d3dPerFrameCB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
		PerFrameCB* perFrameCB = static_cast<PerFrameCB*>(mappedResource.pData);

		//update the data
		perFrameCB->dirLight = m_dirLight;
		perFrameCB->spotLight = m_spotLight;
		for (int i = 0; i < m_pointLights.size(); i++) {
		perFrameCB->pointLights[i] = m_pointLights[i];
		}
		perFrameCB->eyePosW = m_camera.GetPosition();

		// enable gpu access
		m_d3dContext->Unmap(m_d3dPerFrameCB.Get(), 0);
	}


	// RarelyChangedCB
	{
		if (m_isLightControlDirty)
		{
			D3D11_MAPPED_SUBRESOURCE mappedResource;
			ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

			// disable gpu access
			XTEST_D3D_CHECK(m_d3dContext->Map(m_d3dRarelyChangedCB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
			RarelyChangedCB* rarelyChangedCB = static_cast<RarelyChangedCB*>(mappedResource.pData);

			//update the data
			rarelyChangedCB->useDirLight = m_lightsControl.useDirLight;
			rarelyChangedCB->usePointLight = m_lightsControl.usePointLight;
			rarelyChangedCB->useSpotLight = m_lightsControl.useSpotLight;

			// enable gpu access
			m_d3dContext->Unmap(m_d3dRarelyChangedCB.Get(), 0);

			m_d3dContext->PSSetConstantBuffers(2, 1, m_d3dRarelyChangedCB.GetAddressOf());
			m_isLightControlDirty = false;

		}
	}

	m_d3dAnnotation->EndEvent();
}


void TexturesDemoApp::RenderScene()
{
	m_d3dAnnotation->BeginEvent(L"render-scene");

	// clear the frame
	m_d3dContext->ClearDepthStencilView(m_depthBufferView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
	m_d3dContext->ClearRenderTargetView(m_backBufferView.Get(), DirectX::Colors::DarkGray);

	// set the shaders and the input layout
	m_d3dContext->RSSetState(m_rasterizerState.Get());
	m_d3dContext->IASetInputLayout(m_inputLayout.Get());
	m_d3dContext->VSSetShader(m_vertexShader.Get(), nullptr, 0);
	m_d3dContext->PSSetShader(m_pixelShader.Get(), nullptr, 0);

	m_d3dContext->PSSetConstantBuffers(1, 1, m_d3dPerFrameCB.GetAddressOf());
	m_d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_d3dContext->PSSetSamplers(0, 1, m_textureSampler.GetAddressOf());

	// draw plane
	{
		// bind the constant data to the vertex shader
		m_d3dContext->VSSetConstantBuffers(0, 1, m_plane.d3dPerObjectCB.GetAddressOf());
		// bind the constant data to the pixel shader
		m_d3dContext->PSSetConstantBuffers(0, 1, m_plane.d3dPerObjectCB.GetAddressOf());
		// bind the texture data to the pixel shader
		m_d3dContext->PSSetShaderResources(0, 1, m_plane.d3dAlbedoMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(1, 1, m_plane.d3dNormalMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(2, 1, m_plane.d3dGlossMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(3, 1, m_plane.d3dSkinMap.GetAddressOf());

		// set what to draw
		UINT stride = sizeof(mesh::MeshData::Vertex);
		UINT offset = 0;
		m_d3dContext->IASetVertexBuffers(0, 1, m_plane.d3dVertexBuffer.GetAddressOf(), &stride, &offset);
		m_d3dContext->IASetIndexBuffer(m_plane.d3dIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		m_d3dContext->DrawIndexed(UINT(m_plane.mesh.indices.size()), 0, 0);
	}


	// draw sphere
	{
		// bind the constant data to the vertex shader
		m_d3dContext->VSSetConstantBuffers(0, 1, m_sphere.d3dPerObjectCB.GetAddressOf());
		// bind the constant data to the pixel shader
		m_d3dContext->PSSetConstantBuffers(0, 1, m_sphere.d3dPerObjectCB.GetAddressOf());
		// bind the texture data to the pixel shader
		m_d3dContext->PSSetShaderResources(0, 1, m_sphere.d3dAlbedoMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(1, 1, m_sphere.d3dNormalMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(2, 1, m_sphere.d3dGlossMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(3, 1, m_sphere.d3dSkinMap.GetAddressOf());

		// set what to draw
		UINT stride = sizeof(mesh::MeshData::Vertex);
		UINT offset = 0;
		m_d3dContext->IASetVertexBuffers(0, 1, m_sphere.d3dVertexBuffer.GetAddressOf(), &stride, &offset);
		m_d3dContext->IASetIndexBuffer(m_sphere.d3dIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		m_d3dContext->DrawIndexed(UINT(m_sphere.mesh.indices.size()), 0, 0);
	}

	// draw torus
	{
		// bind the constant data to the vertex shader
		m_d3dContext->VSSetConstantBuffers(0, 1, m_torus.d3dPerObjectCB.GetAddressOf());
		// bind the constant data to the pixel shader
		m_d3dContext->PSSetConstantBuffers(0, 1, m_torus.d3dPerObjectCB.GetAddressOf());
		// bind the texture data to the pixel shader
		m_d3dContext->PSSetShaderResources(0, 1, m_torus.d3dAlbedoMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(1, 1, m_torus.d3dNormalMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(2, 1, m_torus.d3dGlossMap.GetAddressOf());
		m_d3dContext->PSSetShaderResources(3, 1, m_torus.d3dSkinMap.GetAddressOf());

		// set what to draw
		UINT stride = sizeof(mesh::MeshData::Vertex);
		UINT offset = 0;
		m_d3dContext->IASetVertexBuffers(0, 1, m_torus.d3dVertexBuffer.GetAddressOf(), &stride, &offset);
		m_d3dContext->IASetIndexBuffer(m_torus.d3dIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		m_d3dContext->DrawIndexed(UINT(m_torus.mesh.indices.size()), 0, 0);
	}


	XTEST_D3D_CHECK(m_swapChain->Present(0, 0));

	m_d3dAnnotation->EndEvent();
}

